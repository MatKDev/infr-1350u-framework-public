#include "Row.h"
#include "Vehicle.h"
#include "River.h"
#include "RiverLog.h"

std::vector<Row*> Row::rowList = std::vector<Row*>();

Row::Row(int a_rowNumber)
{
	if (int a = rand() % 2)
		direction = glm::vec3(-1.0f, 0.0f, 0.0f);
	else
		direction = glm::vec3(1.0f, 0.0f, 0.0f);

	speed = rand() % (70 - 20) + 20;

	yPos = 40 + (20 * a_rowNumber); //start at row 2 and step up by the height of one row each time

	//randomly figure out what type the row should be
	int randNum = rand() % 10 + 1;

	if (randNum <= 2)
		rowType = Types::safe;
	else if (randNum <= 5)
		rowType = Types::river;
	else //6-10
		rowType = Types::road;

	if (rowType == Types::road)
	{
		int initialXPos = rand() % 600;
		int prevXPos = initialXPos;
		for (int i = 0; i < 5; i++)
		{
			int newXPos = prevXPos + rand() % (140 - 90) + 90;
			if (newXPos > 600)
				newXPos -= 600;

			Vehicle* newVehicle = new Vehicle(direction, glm::vec3(newXPos, yPos, 0.0f), speed);
			prevXPos = newXPos;
		}
	}
	else if (rowType == Types::river)
	{
		River* newRiver = new River(glm::vec3(0.0f, yPos, 0.0f));

		int initialXPos = rand() % 600;
		int prevXPos = initialXPos;
		for (int i = 0; i < 5; i++)
		{
			int newXPos = prevXPos + rand() % (140 - 90) + 90;
			if (newXPos > 600)
				newXPos -= 600;

			RiverLog* newRiverLog = new RiverLog(direction, glm::vec3(newXPos, yPos, 0.0f), speed);
			prevXPos = newXPos;
		}
	}
	//else safe row (we dont actually need any code for this since it's just a row of nothing)
	
	rowList.push_back(this);
}
