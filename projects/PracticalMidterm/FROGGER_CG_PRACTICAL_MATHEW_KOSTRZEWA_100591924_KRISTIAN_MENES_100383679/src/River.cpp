#include "River.h"
#include <vector>

std::vector<River*> River::riverList = std::vector<River*>();
Mesh::Sptr River::mesh = nullptr;
bool River::isMeshCreated = false;

River::River(glm::vec3 a_position)
{
	if (!isMeshCreated)
	{
		// Create our 4 vertices
		Vertex vertices[4] = {
			//       Position                   Color              
			//    x      y     z         r    g     b     a
			{{ 0.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f, 1.0f }},
			{{  1.0f, 0.0f, 0.0f }, { 0.0f, 0.0f, 1.0f, 1.0f }},
			{{ 0.0f,  1.0f, 0.0f }, { 0.0f, 0.0f, 1.0f, 1.0f }},
			{{  1.0f,  1.0f, 0.0f }, { 0.0f, 0.0f, 1.0f, 1.0f }}
		};

		// Create our 6 indices
		uint32_t indices[6] = {
			0, 1, 2,
			2, 1, 3
		};

		// Create a new mesh from the data
		mesh = std::make_shared<Mesh>(vertices, 4, indices, 6);

		isMeshCreated = true;
	}
	
	position = a_position;

	size = glm::vec3(600.0f, 20.0f, 1.0f);

	transform = glm::mat4(1.0f);
	transform = glm::translate(transform, position);
	transform = glm::scale(transform, size);

	riverList.push_back(this);
}
