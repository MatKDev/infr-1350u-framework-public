#include "Frog.h"
#include <iostream>
#include <algorithm>

Frog::Frog()
{
	// Create our 4 vertices
	Vertex vertices[4] = {
		//       Position                   Color              
		//    x      y     z         r    g     b     a
		{{ 0.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }},
		{{  1.0f, 0.0f, 0.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }},
		{{ 0.0f,  1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }},
		{{  1.0f,  1.0f, 0.0f }, { 0.0f, 1.0f, 0.0f, 1.0f }}
	};
	
	// Create our 6 indices
	uint32_t indices[6] = {
		0, 1, 2,
		2, 1, 3
	};

	transform = glm::mat4(1.0f);

	// Create a new mesh from the data
	mesh = std::make_shared<Mesh>(vertices, 4, indices, 6);

	resetPos();
	
	size = glm::vec3(15.0f, 20.0f, 1.0f);
}

void Frog::update(float dt)
{
	transform = glm::mat4(1.0f);
	transform = glm::translate(transform, position);
	transform = glm::scale(transform, size);

	position.x = std::max(0.0f, position.x);
	position.x = std::min(600.0f - size.x, position.x);
}

void Frog::moveUp()
{
	position.y += 20.0f;
}

void Frog::moveLeft()
{
	position.x -= 15.0f;
}

void Frog::moveRight()
{
	position.x += 15.0f;
}

bool Frog::isCollidingWith(MyGameObject* otherObject)
{
	if (position.x < otherObject->position.x + otherObject->size.x &&
		position.x + size.x > otherObject->position.x &&
		position.y < otherObject->position.y + otherObject->size.y &&
		position.y + size.y > otherObject->position.y)
	{
		return true;
	}

	return false;
}

void Frog::resetPos()
{
	position = glm::vec3(300.0f, 0.0f, 0.0f);
}