#pragma once
#include "MyGameObject.h"

class RiverLog : public MyGameObject
{
public:
	static std::vector<RiverLog*> riverLogList;

	RiverLog(glm::vec3 a_direction, glm::vec3 a_position, float a_speed);

	void update(float dt);

	glm::vec3 direction;
	float speed;

	static Mesh::Sptr mesh;
	static bool isMeshCreated;
};