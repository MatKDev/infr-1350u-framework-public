#include "RiverLog.h"
#include <vector>
#include <algorithm>

std::vector<RiverLog*> RiverLog::riverLogList = std::vector<RiverLog*>();
Mesh::Sptr RiverLog::mesh = nullptr;
bool RiverLog::isMeshCreated = false;

RiverLog::RiverLog(glm::vec3 a_direction, glm::vec3 a_position, float a_speed)
{
	if (!isMeshCreated)
	{
		// Create our 4 vertices
		Vertex vertices[4] = {
			//       Position                   Color              
			//    x      y     z         r    g     b     a
			{{ 0.0f, 0.0f, 0.0f },   { 0.6f, 0.3f, 0.0f, 1.0f }},
			{{  1.0f, 0.0f, 0.0f },  { 0.6f, 0.3f, 0.0f, 1.0f }},
			{{ 0.0f,  1.0f, 0.0f },  { 0.6f, 0.3f, 0.0f, 1.0f }},
			{{  1.0f,  1.0f, 0.0f }, { 0.6f, 0.3f, 0.0f, 1.0f }}
		};

		// Create our 6 indices
		uint32_t indices[6] = {
			0, 1, 2,
			2, 1, 3
		};

		// Create a new mesh from the data
		mesh = std::make_shared<Mesh>(vertices, 4, indices, 6);

		isMeshCreated = true;
	}

	transform = glm::mat4(1.0f);
	speed = a_speed;
	direction = a_direction;
	position = a_position;

	size = glm::vec3(60.0f, 20.0f, 1.0f);

	riverLogList.push_back(this);
}

void RiverLog::update(float dt)
{
	position += direction * speed * dt;

	transform = glm::mat4(1.0f);
	transform = glm::translate(transform, position);
	transform = glm::scale(transform, size);

	if (position.x < 0 - size.x)
		position.x += 600 + size.x;
	else if (position.x > 600)
		position.x -= 600 + size.x;
}
