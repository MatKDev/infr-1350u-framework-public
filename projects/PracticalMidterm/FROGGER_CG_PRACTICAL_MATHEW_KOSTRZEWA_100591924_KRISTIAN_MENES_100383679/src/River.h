#pragma once
#include "MyGameObject.h"

class River : public MyGameObject
{
public:
	static std::vector<River*> riverList;

	River(glm::vec3 a_position);

	static Mesh::Sptr mesh;
	static bool isMeshCreated;
};