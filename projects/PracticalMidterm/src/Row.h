#pragma once
#include "GLM/glm.hpp"
#include <GLM/gtc/matrix_transform.hpp>
#include <vector>

class Row
{
public:
	enum class Types
	{
		safe,
		road,
		river
	};
	Types rowType;
	
	static std::vector<Row*> rowList;

	Row(int a_rowNumber);

	glm::vec3 direction;
	float speed;
	int yPos;
};

