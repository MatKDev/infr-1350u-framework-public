#pragma once
#include "Mesh.h"
#include "GLM/glm.hpp"
#include <GLM/gtc/matrix_transform.hpp>
#include <vector>

class MyGameObject
{
public:	
	glm::mat4 transform;

	glm::vec3 position;
	glm::vec3 size;
};