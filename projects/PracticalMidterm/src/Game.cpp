#include "Game.h"
#include "SceneManager.h"
#include "MeshRenderer.h"
#include "Material.h"
#include "Logging.h"
#include "Row.h"
#include "River.h"
#include "RiverLog.h"

#include <stdexcept>

#define IMGUI_IMPL_OPENGL_LOADER_GLAD
#include "imgui.h"
#include "imgui_impl_opengl3.cpp"
#include "imgui_impl_glfw.cpp"
#include <GLM/gtc/matrix_transform.hpp>
#include <functional>


struct TempTransform {
	glm::vec3 Position;
	glm::vec3 EulerRotation;
	glm::vec3 Scale;

	glm::mat4 GetWorldTransform() const {
		return
			glm::translate(glm::mat4(1.0f), Position) *
			glm::mat4_cast(glm::quat(glm::radians(EulerRotation))) *
			glm::scale(glm::mat4(1.0f), Scale)
			;
	}
};

struct UpdateBehaviour {
	std::function<void(entt::entity e, float dt)> Function;
};

Frog* frog;

void key_callback(GLFWwindow* window, int key, int scancode, int action, int mods)
{
	if ((key == GLFW_KEY_W || key == GLFW_KEY_UP) && action == GLFW_PRESS)
		frog->moveUp();
	else if ((key == GLFW_KEY_A || key == GLFW_KEY_LEFT) && action == GLFW_PRESS)
		frog->moveLeft();
	else if ((key == GLFW_KEY_D || key == GLFW_KEY_RIGHT) && action == GLFW_PRESS)
		frog->moveRight();
}

/*
	Handles debug messages from OpenGL
	https://www.khronos.org/opengl/wiki/Debug_Output#Message_Components
	@param source    Which part of OpenGL dispatched the message
	@param type      The type of message (ex: error, performance issues, deprecated behavior)
	@param id        The ID of the error or message (to distinguish between different types of errors, like nullref or index out of range)
	@param severity  The severity of the message (from High to Notification)
	@param length    The length of the message
	@param message   The human readable message from OpenGL
	@param userParam The pointer we set with glDebugMessageCallback (should be the game pointer)
*/
void GlDebugMessage(GLenum source, GLenum type, GLuint id, GLenum severity, GLsizei length, const GLchar* message, const void* userParam) {
	switch (severity) {
	case GL_DEBUG_SEVERITY_LOW:          LOG_INFO(message); break;
	case GL_DEBUG_SEVERITY_MEDIUM:       LOG_WARN(message); break;
	case GL_DEBUG_SEVERITY_HIGH:         LOG_ERROR(message); break;
#ifdef LOG_GL_NOTIFICATIONS
	case GL_DEBUG_SEVERITY_NOTIFICATION: LOG_INFO(message); break;
#endif
	default: break;
	}
}

void GlfwWindowResizedCallback(GLFWwindow* window, int width, int height) {
	glViewport(0, 0, width, height);

	Game* game = (Game*)glfwGetWindowUserPointer(window);
	if (game) {
		game->Resize(width, height);
	}
}

Game::Game() :
	myWindow(nullptr),
	myWindowTitle("Game"),
	myClearColor(glm::vec4(0.1f, 0.1f, 0.1f, 1.0f))
{ }

Game::~Game() { }

void Game::Run()
{
	Initialize();

	LoadContent();

	static float prevFrame = glfwGetTime();
	
	// Run as long as the window is open
	while (!glfwWindowShouldClose(myWindow)) {

		float thisFrame = glfwGetTime();
		float deltaTime = thisFrame - prevFrame;

		Update(deltaTime);
		
		Draw(deltaTime);

		// Present our image to windows
		glfwSwapBuffers(myWindow);

		// Poll for events from windows (clicks, keypressed, closing, all that)
		glfwPollEvents();

		prevFrame = thisFrame;
	}

	LOG_INFO("Shutting down...");

	UnloadContent();

	ShutdownImGui();
	Shutdown();
}

void Game::Initialize() {
	// Initialize GLFW
	if (glfwInit() == GLFW_FALSE) {
		std::cout << "Failed to initialize GLFW" << std::endl;
		throw std::runtime_error("Failed to initialize GLFW");
	}
	
	// Enable transparent backbuffers for our windows (note that Windows expects our colors to be pre-multiplied with alpha)
	glfwWindowHint(GLFW_TRANSPARENT_FRAMEBUFFER, true);

	// Create a new GLFW window
	myWindow = glfwCreateWindow(1000, 800, myWindowTitle, nullptr, nullptr);

	// Tie our game to our window, so we can access it via callbacks
	glfwSetWindowUserPointer(myWindow, this);

	// Set our window resized callback
	glfwSetWindowSizeCallback(myWindow, GlfwWindowResizedCallback);

	// We want GL commands to be executed for our window, so we make our window's context the current one
	glfwMakeContextCurrent(myWindow);

	// Let glad know what function loader we are using (will call gl commands via glfw)
	if (gladLoadGLLoader((GLADloadproc)glfwGetProcAddress) == 0) {
		std::cout << "Failed to initialize Glad" << std::endl;
		throw std::runtime_error("Failed to initialize GLAD");
	}

	// Log our renderer and OpenGL version
	LOG_INFO(glGetString(GL_RENDERER));
	LOG_INFO(glGetString(GL_VERSION));

	// Enable debugging, and route messages to our callback
	glEnable(GL_DEBUG_OUTPUT);
	glDebugMessageCallback(GlDebugMessage, this);

	glfwSetKeyCallback(myWindow, key_callback);

	srand(time(NULL));

	projection = glm::ortho(0.0f, 600.0f, 0.0f, 600.0f, -1.0f, 1.0f);
}

void Game::Shutdown() {
	glfwTerminate();
}
 
void Game::LoadContent() {
	frog = new Frog();

	myShader = std::make_shared<Shader>();
	myShader->Load("basicShader.vs", "basicShader.fs");

	for (int i = 0; i < 27; i++)
		Row* newRow = new Row(i);
}


void Game::UnloadContent() {

}

void Game::InitImGui() {
}

void Game::ShutdownImGui() {
}

void Game::ImGuiNewFrame() {
}

void Game::ImGuiEndFrame() {
}

void Game::Update(float deltaTime) {
	frog->update(deltaTime);
	
	if (frog->position.y >= 600)
		ResetGame();

	for (int i = 0; i < Vehicle::vehicleList.size(); i++)
	{
		Vehicle::vehicleList[i]->update(deltaTime);
		if (frog->isCollidingWith(Vehicle::vehicleList[i]))
			frog->resetPos();
	}

	for (int i = 0; i < RiverLog::riverLogList.size(); i++)
	{
		RiverLog::riverLogList[i]->update(deltaTime);
	}

	for (int i = 0; i < River::riverList.size(); i++)
	{
		//check if there's a collision with any rivers
		if (frog->isCollidingWith(River::riverList[i]))
		{
			bool isOnRiverLog = false;
			//check if the player is on a log
			for (int i = 0; i < RiverLog::riverLogList.size(); i++)
			{
				//check if there's a collision with any logs
				if (frog->isCollidingWith(RiverLog::riverLogList[i]))
				{
					frog->position += RiverLog::riverLogList[i]->direction * RiverLog::riverLogList[i]->speed * deltaTime;
					isOnRiverLog = true;
					break;
				}
			}
			//collision with the river but no logs
			if (!isOnRiverLog)
				frog->resetPos();
			
			break;
		}
	}
}

void Game::Draw(float deltaTime) {
	// Clear our screen every frame
	glClearColor(myClearColor.x, myClearColor.y, myClearColor.z, myClearColor.w);
	glClear(GL_COLOR_BUFFER_BIT);

	myShader->Bind();
	
	for (int i = 0; i < River::riverList.size(); i++)
	{
		myShader->SetUniform("MVP", projection * River::riverList[i]->transform);
		River::mesh->Draw();
	}

	for (int i = 0; i < RiverLog::riverLogList.size(); i++)
	{
		myShader->SetUniform("MVP", projection * RiverLog::riverLogList[i]->transform);
		RiverLog::mesh->Draw();
	}
	
	myShader->SetUniform("MVP", projection * frog->transform);
	frog->mesh->Draw();

	for (int i = 0; i < Vehicle::vehicleList.size(); i++)
	{
		myShader->SetUniform("MVP", projection * Vehicle::vehicleList[i]->transform);
		Vehicle::vehicleList[i]->mesh->Draw();
	}

}

void Game::DrawGui(float deltaTime) {
}

void Game::Resize(int newWidth, int newHeight) {
}

void Game::ResetGame()
{
	River::riverList.clear();
	RiverLog::riverLogList.clear();
	Vehicle::vehicleList.clear();
	Row::rowList.clear();

	frog->resetPos();
	
	for (int i = 0; i < 27; i++)
		Row* newRow = new Row(i);
}
