#include "Vehicle.h"
#include <vector>
#include <algorithm>

std::vector<Vehicle*> Vehicle::vehicleList = std::vector<Vehicle*>();

Vehicle::Vehicle(glm::vec3 a_direction, glm::vec3 a_position, float a_speed)
{
	//get random colors
	float red = rand() / double(RAND_MAX);
	float green = rand() / double(RAND_MAX);
	float blue = rand() / double(RAND_MAX);

	//make sure colors arent too dark (or else they'll blend in with background
	red   = std::max(red,   0.2f);
	green = std::max(green, 0.2f);
	blue  = std::max(blue,  0.2f);

	// Create our 4 vertices
	Vertex vertices[4] = {
		//       Position                   Color              
		//    x      y     z         r    g     b     a
		{{ 0.0f, 0.0f, 0.0f }, { red, green, blue, 1.0f }},
		{{  1.0f, 0.0f, 0.0f }, { red, green, blue, 1.0f }},
		{{ 0.0f,  1.0f, 0.0f }, { red, green, blue, 1.0f }},
		{{  1.0f,  1.0f, 0.0f }, { red, green, blue, 1.0f }}
	};

	// Create our 6 indices
	uint32_t indices[6] = {
		0, 1, 2,
		2, 1, 3
	};

	transform = glm::mat4(1.0f);
	speed = a_speed;
	direction = a_direction;
	position = a_position;

	size = glm::vec3(40.0f, 20.0f, 1.0f);

	// Create a new mesh from the data
	mesh = std::make_shared<Mesh>(vertices, 4, indices, 6);

	vehicleList.push_back(this);
}

void Vehicle::update(float dt)
{
	position += direction * speed * dt;

	transform = glm::mat4(1.0f);
	transform = glm::translate(transform, position);
	transform = glm::scale(transform, size);

	if (position.x < 0 - size.x)
		position.x += 600 + size.x;
	else if (position.x > 600)
		position.x -= 600 + size.x;
}
