#include "MyGameObject.h"

class Vehicle : public MyGameObject
{
public:
	static std::vector<Vehicle*> vehicleList;

	Vehicle(glm::vec3 a_direction, glm::vec3 a_position, float a_speed);

	void update(float dt);

	glm::vec3 direction;
	float speed;

	Mesh::Sptr mesh;
};