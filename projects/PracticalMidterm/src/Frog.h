#pragma once
#include "MyGameObject.h"

class Frog : public MyGameObject
{
public:
	Frog();

	void update(float dt);

	void moveUp();
	void moveLeft();
	void moveRight();

	void resetPos();

	bool isCollidingWith(MyGameObject* otherObject);

	Mesh::Sptr mesh;
};