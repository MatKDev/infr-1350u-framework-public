#include "Mesh.h"
#include "fstream"
#include <string>

struct Face
{
	Face()
	{
		UVs[0] = 0;
		UVs[1] = 0;
		UVs[2] = 0;

		positions[0] = 0;
		positions[1] = 0;
		positions[2] = 0;

		normals[0] = 0;
		normals[1] = 0;
		normals[2] = 0;
	}

	Face(
		unsigned int u1, unsigned int u2, unsigned int u3,
		unsigned int p1, unsigned int p2, unsigned int p3,
		unsigned int n1, unsigned int n2, unsigned int n3)
	{

		UVs[0] = u1;
		UVs[1] = u2;
		UVs[2] = u3;
		
		positions[0] = p1;
		positions[1] = p2;
		positions[2] = p3;

		normals[0] = n1;
		normals[1] = n2;
		normals[2] = n3;
	}

	unsigned int UVs[3];
	unsigned int positions[3];
	unsigned int normals[3];

};

Mesh::Mesh(std::string filepath) {
	LoadObj(filepath);
}

Mesh::~Mesh() {
	// Clean up our buffers
	glDeleteBuffers(1, &myVBO);
	// Clean up our VAO
	glDeleteVertexArrays(1, &myVAO);
}

void Mesh::Draw() {
	// Bind the mesh
	glBindVertexArray(myVAO);

	glDrawArrays(GL_TRIANGLES, 0, combinedVertexData.size());
}

void Mesh::LoadObj(std::string filepath)
{
	// Open our file in binary mode
	std::ifstream file;
	file.open(filepath, std::ios::binary);
	// If our file fails to open, we will throw an error
	if (!file)
		throw new std::runtime_error("Failed to open file");

	std::vector<Face>      facesFromFile;
	std::vector<glm::vec2> UVsFromFile;
	std::vector<glm::vec3> positionsFromFile;
	std::vector<glm::vec3> normalsFromFile;

	Face  tempFace;
	float tempX, tempY, tempZ;

	std::string line;
	//read in the data from the .obj file
	while (std::getline(file, line)) 
	{
		if (line[0] == 'f')
		{
			std::sscanf(
				line.c_str(), "f %u/%u/%u %u/%u/%u %u/%u/%u",
				&tempFace.positions[0], &tempFace.UVs[0], &tempFace.normals[0],
				&tempFace.positions[1], &tempFace.UVs[1], &tempFace.normals[1],
				&tempFace.positions[2], &tempFace.UVs[2], &tempFace.normals[2]);
			
			facesFromFile.push_back(tempFace);
		}
		else if (line[0] == 'v' && line[1] == 't')
		{
			std::sscanf(line.c_str(), "vt %f %f", &tempX, &tempY);
			UVsFromFile.push_back(glm::vec2(tempX, tempY));

		}
		else if (line[0] == 'v' && line[1] == ' ')
		{
			std::sscanf(line.c_str(), "v %f %f %f", &tempX, &tempY, &tempZ);
			positionsFromFile.push_back(glm::vec3(tempX, tempY, tempZ));
		}
		else if (line[0] == 'v' && line[1] == 'n')
		{
			std::sscanf(line.c_str(), "vn %f %f %f", &tempX, &tempY, &tempZ);
			normalsFromFile.push_back(glm::vec3(tempX, tempY, tempZ));
		}
	}
	file.close();

	//process the data
	for (unsigned i = 0; i < facesFromFile.size(); i++)
	{
		for (unsigned int f = 0; f < 3; f++)
		{
			glm::vec2 uv = UVsFromFile[facesFromFile[i].UVs[f] - 1];

			combinedVertexData.push_back(uv[0]);
			combinedVertexData.push_back(uv[1]);

			glm::vec3 pos = positionsFromFile[facesFromFile[i].positions[f] - 1];

			combinedVertexData.push_back(pos[0]);
			combinedVertexData.push_back(pos[1]);
			combinedVertexData.push_back(pos[2]);

			glm::vec3 normal = normalsFromFile[facesFromFile[i].normals[f] - 1];

			combinedVertexData.push_back(pos[0]);
			combinedVertexData.push_back(pos[1]);
			combinedVertexData.push_back(pos[2]);
		}
	}

	// Create and bind our vertex array
	glCreateVertexArrays(1, &myVAO);
	glBindVertexArray(myVAO);

	// Create 2 buffers, 1 for vertices and the other for indices
	glCreateBuffers(1, &myVBO);

	// Bind and buffer our position data
	glBindBuffer(GL_ARRAY_BUFFER, myVBO);
	glBufferData(GL_ARRAY_BUFFER, combinedVertexData.size() * sizeof(float), combinedVertexData.data(), GL_STATIC_DRAW);

	//set texture UVs
	glVertexAttribPointer(0, 2, GL_FLOAT, false, sizeof(float) * 8, (void*)0);
	glEnableVertexAttribArray(0);
	
	//set positions
	glVertexAttribPointer(1, 3, GL_FLOAT, false, sizeof(float) * 8, (void*)8);
	glEnableVertexAttribArray(1);

	//normals
	glVertexAttribPointer(2, 3, GL_FLOAT, false, sizeof(float) * 8, (void*)20);
	glEnableVertexAttribArray(2);

	// Unbind our VAO
	glBindVertexArray(0);
}
