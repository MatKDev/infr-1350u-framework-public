#version 410

layout (location = 0) in vec2 inTextureUVs;
layout (location = 1) in vec3 inPosition;
layout (location = 2) in vec3 inNormals;

uniform mat4 a_ModelViewProjection;

void main() 
{
    gl_Position = a_ModelViewProjection * vec4(inPosition, 1);
}