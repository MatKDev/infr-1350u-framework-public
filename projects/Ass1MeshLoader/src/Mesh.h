#pragma once

#include <glad/glad.h>
#include <GLM/glm.hpp> // For vec3 and vec4
#include <cstdint> // Needed for uint32_t
#include <memory> // Needed for smart pointers
#include <vector>
#include <iostream>

class Mesh {
public:
	// Creates a new mesh from the given vertices and indices
	Mesh(std::string filepath);
	~Mesh();

	// Draws this mesh
	void Draw();

	void LoadObj(std::string filepath);

private:
	// Our GL handle for the Vertex Array Object
	GLuint myVAO;
	// 0 is vertices, 1 is indices
	GLuint myVBO;
	// The number of vertices and indices in this mesh
	size_t myVertexCount, myIndexCount;

	std::vector<float> combinedVertexData;
};

// Shorthand for shared_ptr
typedef std::shared_ptr<Mesh> Mesh_sptr;