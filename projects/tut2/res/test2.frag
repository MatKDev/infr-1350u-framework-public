#version 410

layout (location = 0) in vec4 inColor;
layout (location = 0) out vec4 outColor;

void main() 
{
    outColor = inColor * vec4(0.2f, 0.8f, 0.4f, 1.0f);
}